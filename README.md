# Nativefier Docker Image

## This repository maintains the Docker image for the official [Nativefier](https://github.com/jiahaog/nativefier) repository.

#### Docker Hub - [https://hub.docker.com/r/snpranav/nativefier](https://hub.docker.com/r/snpranav/nativefier)
### Pre-requisites
- Docker (Install it [here](https://docs.docker.com/get-docker/))

To build your first nativefier application using Docker, run
```bash
docker run -v $TARGET-PATH:/target snpranav/nativefier https://my-web-app.com/ /target/
```
You can also pass nativefier flags, and mount additional volumes to provide local files. For example, to use a icon:
```bash
docker run -v $PATH_TO_ICON/:/src -v $TARGET-PATH:/target snpranav/nativefier --icon /src/icon.png --name WhatsApp -p linux -a x64 https://my-web-app.com/ /target/
```
For more flags, please refer to the [official source code repository](https://github.com/jiahaog/nativefier).

**Do NOT send pull requests for any source code changes. Please send them to the [official source code repository](https://github.com/jiahaog/nativefier). Pull requests to update .gitlab-ci.yml file (CI/CD pipeline) are welcome.**

## License

[MIT](LICENSE.md)
